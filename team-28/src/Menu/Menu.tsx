import * as React from 'react';
import './Menu.css';

export class Menu extends React.Component<any, any>
{
    constructor(props: any)
    {
        super(props)
        this.state = {
            active: false
        }
    }

    handleClick = () => {
        this.setState({ active: !this.state.active});
    }
    render()
    {
        return (
            <nav className="menu">
                <div className={ this.state.active ? 'menu-toggle active' : 'menu-toggle' } onClick={ this.handleClick }>
                    <div className="inner">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <div className={ this.state.active ? 'nav nav-items active' : 'nav nav-items '}>
                    { this.props.children}
                </div>
            </nav>

        )
    }
}