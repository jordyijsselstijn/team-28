import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BlogPost, iBlogPost } from './BlogPost';
import { BlogPostContainer } from './BlogPost/BlogPostContainer';
import { Hero } from './Hero';
import { Menu } from './Menu'; 

class App extends Component {
  render() {
    let Posts: iBlogPost[] = [
      { slug: '1', imageUrl: 'https://via.placeholder.com/150/92c952', content: "accusamus beatae ad facilis cum similique qui sunt", title: "test" },
      { slug: '2', imageUrl: 'https://via.placeholder.com/150/92c952', content: "accusamus beatae ad facilis cum similique qui sunt", title: "accusamus beatae ad facilis cum similique qui sunt" },
      { slug: '3', imageUrl: 'https://via.placeholder.com/150/92c952', content: "accusamus beatae ad facilis cum similique qui sunt", title: "accusamus beatae ad facilis cum similique qui sunt" },
      { slug: '4', imageUrl: 'https://via.placeholder.com/150/92c952', content: "accusamus beatae ad facilis cum similique qui sunt", title: "accusamus beatae ad facilis cum similique qui sunt" },
      { slug: '5', imageUrl: 'https://via.placeholder.com/150/92c952', content: "accusamus beatae ad facilis cum similique qui sunt", title: "accusamus beatae ad facilis cum similique qui sunt" }
    ];

    let menuLinks = Posts.map((value: iBlogPost, key: number) => {
      return <li key={key}><a href={ '#' + value.slug }>{value.title}</a></li>
    });

    return (
      <div className="App">
        <Menu> 
          <ul>
            {menuLinks}
          </ul>
        </Menu>
        <Hero main="Team" second="28" subTitle="Developing in cooperation with <span>Rotterdam</span>"/>
        <BlogPostContainer Posts={Posts} /> 
      </div>
    );
  }
}

export default App;
