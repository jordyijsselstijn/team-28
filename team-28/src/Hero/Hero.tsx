import * as React from 'react';
import './hero.css';

export class Hero extends React.Component<any, any>
{
    constructor(props: any)
    {
        super(props);
        this.state = {
            radius: 0
        }
    }

    componentDidMount()
    {
        let timeout: any;
        // Listen for resize events
        window.addEventListener('scroll', ( event: any ) => {
            this.setState({ radius: event.target.scrollingElement.scrollTop})
        }, false);
    }
    render()
    {
        let opacity = (1 - ( this.state.radius / 500) );
        let styles = {
            main: {
                transform: 'translateX( -' + ( this.state.radius )+ 'px )',
                opacity: opacity < 0 ? 0 : opacity
            },
            second: {
                transform: 'translateX( ' + (this.state.radius )+'px )',
                opacity: opacity < 0 ? 0 : opacity
            },
            subtitle: {
                opacity: opacity < 0 ? 0 : opacity
            }
        }
        return (
            <div className="hero-container">
                <div className="hero">   
                    <div className="main">
                        <h1 style={ styles.main }>{this.props.main}</h1>
                        <h1 style={ styles.second } >{this.props.second}</h1>
                    </div>
                    <h3 style={ styles.subtitle } className="subtitle" dangerouslySetInnerHTML={ { __html: this.props.subTitle }}></h3>
                </div>
            </div>

        )
    }
}