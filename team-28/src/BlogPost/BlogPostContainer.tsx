import * as React from 'react';
import { BlogPost, iBlogPost } from './BlogPost';

export const BlogPostContainer = (props: any) => {
    return (props.Posts.map((blogPost: iBlogPost, key: number) => {
        return <BlogPost key={key} Post={blogPost} Reverse={ key % 2 === 0 } />
    }));
}