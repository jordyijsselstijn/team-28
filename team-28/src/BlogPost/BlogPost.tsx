import * as React from 'react';
import './BlogPost.css';

export interface iBlogPost {
    title: string;
    imageUrl: string;
    content: string;
    slug: string;
}

export interface iBlogPostProps {
    Post: iBlogPost;
    Reverse: boolean;
}

export class BlogPost extends React.Component<iBlogPostProps, any> {
    constructor(props: iBlogPostProps)
    {
        super(props);
    }

    render()
    {   
        
        return (
            <section className="BlogPost" id={this.props.Post.slug}>
            { 
                this.props.Reverse && <div>
                        <img src={this.props.Post.imageUrl} alt={this.props.Post.title} />
                        <div>
                            <h1>{ this.props.Post.title }</h1>
                            <p>{this.props.Post.content}</p>
                        </div>
                    </div>
            }
            {
                !this.props.Reverse && <div>
                        <div>
                            <h1>{ this.props.Post.title }</h1>
                            <p>{this.props.Post.content}</p>
                        </div>
                        <img src={this.props.Post.imageUrl} alt={this.props.Post.title} />
                    </div>
            }
            </section>

        )
    }
}